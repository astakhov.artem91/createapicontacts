using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using Terrasoft.Configuration;
using Terrasoft.Core;
using Terrasoft.Core.Factories;
using Terrasoft.Core.Entities;

namespace Consimple.Core.Integration
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CnsContactServiceController
    {
        private UserConnection _uc;
        private IntegrationContacts _service;

        public CnsContactServiceController()
        {
            _uc = (UserConnection)HttpContext.Current.Session["UserConnection"];
            _service = ClassFactory.Get<IntegrationContacts>(new ConstructorArgument("uc", _uc));
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse AddContact(AddContactRequest dto) => _service.AddContact(dto);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse EditContact(EditContactRequest dto) => _service.EditContact(dto);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetByCode/{code}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ContactResponse GetByCode(string code) => _service.GetByCodeContact(code);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetByPhone/{phone}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public ContactResponse GetByPhone(string phone) => _service.GetByPhoneContact(phone);
    }

    public class IntegrationContacts : BaseMediatorService
    {
    	private UserConnection _uc;
        public IntegrationContacts(UserConnection uc) : base(uc) { _uc=uc; }

        public BaseResponse AddContact(AddContactRequest contactRequest) => SetValue<Contact>(contactRequest, false);
        public BaseResponse EditContact(EditContactRequest contactRequest) => Update<Contact>(contactRequest);
        public ContactResponse GetByCodeContact(string code) => GetValueContact<Contact, GetByCodeContact>(new Dictionary<string, object> { { "Code", code } });
        public ContactResponse GetByPhoneContact(string phone) => GetValueContactPhone<Contact, GetByPhoneContact>(new Dictionary<string, object> { { "MobilePhone", phone } });
        
        protected ContactResponse GetValueContact<TEntity, TResponse>(Dictionary<string, object> filter) 
            where TEntity : Entity
            where TResponse : GetByCodeContact, new()
        {
            var entity = _uc.GetFirstOrDefault<TEntity>(filter);
            var response = new TResponse().Build(_uc, entity);
            return response;
        }
        
        protected ContactResponse GetValueContactPhone<TEntity, TResponse>(Dictionary<string, object> filter)
            where TEntity : Entity
            where TResponse : GetByPhoneContact, new()
        {
            var entity = _uc.GetFirstOrDefault<TEntity>(filter);
            var response = new TResponse().Build(_uc, entity);
            return response;
        }

    }


    [KnownType(typeof(GetByPhoneContact))]
    public class GetByPhoneContact
    {
        public ContactResponse Build(UserConnection uc, Entity entity, string key = "OK")
        {
            return GetContactMaper.MapContact(uc, entity);
        }
    }

    [KnownType(typeof(GetByCodeContact))]
    public class GetByCodeContact 
    {
        public  ContactResponse Build(UserConnection uc, Entity entity, string key = "OK")
        {
            return GetContactMaper.MapContact(uc, entity);
        }
    }
    public class ContactResponse
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string MobilePhone { get; set; }
        public string TypeCode { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public bool DoNotUseSms { get; set; }
        public bool DoNotUseEmail { get; set; }
        public virtual string BirthDate
        {
            get { return BirthdateDateTime.ToString(); }
            set { if (!string.IsNullOrEmpty(value)) BirthdateDateTime = DateTime.Parse(value); }
        }
        [IgnoreDataMember]
        public virtual DateTime? BirthdateDateTime { get; set; } = null;
    }

    public static class GetContactMaper
    {
        public static ContactResponse MapContact(UserConnection uc, Entity entityContact)
        {
            var contactInfo = new ContactResponse();

            contactInfo.Code = entityContact.GetColumnValue("Code").ToString();
            contactInfo.Name = entityContact.GetColumnValue("Name").ToString();
          	contactInfo.MobilePhone = entityContact.GetColumnValue("MobilePhone").ToString();
            contactInfo.TypeCode = (Guid)entityContact.GetColumnValue("TypeId") == Consimple.Constants.Contact.ContactType.CONTACT_PERSON ? "ContactPerson" :
                (Guid)entityContact.GetColumnValue("TypeId") == Consimple.Constants.Contact.ContactType.CUSTOMER ? "Customer" : "Employee";
            contactInfo.Email = entityContact.GetColumnValue("Email").ToString();
            contactInfo.Gender = (Guid)entityContact.GetColumnValue("GenderId") == Consimple.Constants.Contact.Gender.FEMALE ? "Female" : "Male";
            contactInfo.Address = entityContact.GetColumnValue("Address").ToString();
            contactInfo.Region = uc.GetById("Region", (Guid)entityContact.GetColumnValue("RegionId"))
                .GetColumnValue("Name").ToString();
            contactInfo.City = uc.GetById("City", (Guid)entityContact.GetColumnValue("CityId"))
                .GetColumnValue("Name").ToString();
            contactInfo.Country = uc.GetById("Country", (Guid)entityContact.GetColumnValue("CountryId"))
                .GetColumnValue("Name").ToString();
            contactInfo.DoNotUseSms = (bool)entityContact.GetColumnValue("DoNotUseSms");
            contactInfo.DoNotUseEmail = (bool)entityContact.GetColumnValue("DoNotUseEmail");
            contactInfo.BirthdateDateTime = (DateTime)entityContact.GetColumnValue("BirthDate");

            return contactInfo;
        }
    }
  
    public class EditContactRequest : AddContactRequest
    {
        protected override Dictionary<string, object> RequiredParams => new Dictionary<string, object>()
        {
            { "Code", Code },
            { "Name", Name },
        };
    }

    public class AddContactRequest : BaseRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string MobilePhone { get; set; }
        public string TypeCode { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        [DataMember]
        public virtual string BirthDate
        {
            get { return BirthdateDateTime.ToString(); }
            set { if (!string.IsNullOrEmpty(value)) BirthdateDateTime = DateTime.Parse(value); }
        }
        [IgnoreDataMember]
        public virtual DateTime? BirthdateDateTime { get; set; } = null;
        public DateTime FirstRegistrationDate { get; set; }
        public bool DoNotUseSms { get; set; }
        public bool DoNotUseEmail { get; set; }

        public override Dictionary<string, object> Params()
        {
            var param = new Dictionary<string, object>();
            param.Add("Code", Code);
            param.Add("Name", Name);
            param.Add("MobilePhone", MobilePhone);
            param.Add("TypeCode", TypeCode);
            param.Add("Email", Email);
            param.Add("Address", Address);
            param.Add("BirthDate", BirthdateDateTime);
            param.Add("FirstRegistrationDate", FirstRegistrationDate);
            param.Add("DoNotUseSms", DoNotUseSms);
            param.Add("DoNotUseEmail", DoNotUseEmail);

            if (Gender == "1" || Gender == "2")
            {
                var gender = Gender == "2" ? Consimple.Constants.Contact.Gender.MALE : 
                    Consimple.Constants.Contact.Gender.FEMALE;
                param.Add("GenderId", gender);
            }
            if (TypeCode == "1" || TypeCode == "2" || TypeCode == "3")
            {
                var type = TypeCode == "1" ? Consimple.Constants.Contact.ContactType.CONTACT_PERSON :
                    TypeCode == "2" ? Consimple.Constants.Contact.ContactType.CUSTOMER :
                    Consimple.Constants.Contact.ContactType.EMPLOYEE;
                param.Add("TypeId", type);
            }

            if (!string.IsNullOrEmpty(City))
                param.Add("CityId", GetByCode<City>(City, "Name"));
            if (!string.IsNullOrEmpty(Country))
                param.Add("CountryId", GetByCode<Country>(Country, "Name"));
            if (!string.IsNullOrEmpty(Region))
                param.Add("RegionId", GetByCode<Region>(Region, "Name"));

            return param;
        }
        public override string BaseCode => Code;
        
        protected override Dictionary<string, object> Filter => new Dictionary<string, object>() { { "Code", BaseCode} };


        protected override Dictionary<string, object> RequiredParams => new Dictionary<string, object>()
        {
            { "Code", Code },
            { "Name", Name },
            { "MobilePhone", MobilePhone },
            { "TypeCode", TypeCode },
        };

        
    }
}