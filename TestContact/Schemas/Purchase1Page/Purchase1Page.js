define("ActivityPageV2", [], function() {
    return {
        entitySchemaName: "Purchase",
        diff: /**SCHEMA_DIFF*/[
            {
                "operation": "insert",
                "parentName": "Header",
                "propertyName": "items",
                "name": "UsrComment",
                "values": {
                    "caption": {"bindTo": "Resources.Strings.UsrComment"},
                    "layout": {
                        "column": 0,
                        "row": 5,
                        "colSpan": 12
                    }
                }
            }
        ]/**SCHEMA_DIFF*/
    };
});